/*
 * Copyright 2015 Bhushan Shah <bshah@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0 as Controls

import org.kde.kirigami 2.4 as Kirigami
import org.kde.mobile.private.screenshot 1.0 as Screenshot

Kirigami.ApplicationWindow {

    id: appUi

    Screenshot.ScreenshotGrabber {
        id: grabber
    }

    pageStack.initialPage: Kirigami.ScrollablePage {
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
        }
        Kirigami.Theme.colorSet: Kirigami.Theme.View

        Timer {
            id: grabTimer
            interval: intervalBox.value * 1000
            running: false
            repeat: false
            onTriggered: img.source = grabber.grabScreenshot()
        }

        title: i18n("Take a screenshot")
        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Image {
                id: img
                Layout.fillWidth: true
                fillMode: Image.PreserveAspectFit

                onSourceChanged: source !== "" ? appUi.visible = true : appUi.visible = false
                visible: source !== ""
            }

            Controls.Label {
                text: i18n("interval")
            }

            Controls.SpinBox {
                id: intervalBox
                value: 5
                Layout.fillWidth: true
                Layout.preferredHeight: Kirigami.Units.gridUnit * 4
            }

            Controls.Button {
                Layout.fillWidth: true
                Layout.preferredHeight: Kirigami.Units.gridUnit * 4
                text: i18n("Take new screenshot")
                onClicked: {
                    grabTimer.start()
                    appUi.visible = false
                }
            }
        }
    }
}
